dxarts461 : Read Me
========================
_DXARTS 461 (Winter 2020): Introduction to digital sound synthesis._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts461a_wi20.quark).

Start by reviewing the Quark installation instructions
[found here](https://github.com/supercollider-quarks/quarks#installing). See
also [Using Quarks](http://doc.sccode.org/Guides/UsingQuarks.html).

With [git](https://git-scm.com/) installed, you can easily install the
[DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts461a_wi20.quark)
directly by running the following line of code in SuperCollider:

    Quarks.install("https://gitlab.com/dxarts/classes/dxarts461a_wi20.quark");



Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts461a_wi20.quark/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 0.1.13

* Update: add 10 page.


Version 0.1.12

* Update: add 09a, 09b & 09c pages.


Version 0.1.11

* Update: add 08a & 08b pages.

* Refactor: 06, navigation title for 07a.


Version 0.1.10

* Update: add 07a & 07b pages.


Version 0.1.9

* Update: add 06 page.


Version 0.1.8

* Update: add 05 page.


Version 0.1.7

* Refactor: MainParadigm -render, add action argument


Version 0.1.6

* Update: add 04a & 04b pages.

* Refactor: 03a & 03b, syntax and typos.


Version 0.1.5

* Update: add 03a - 03c pages.

* Add: MainParadigm class


Version 0.1.4

* Update: add 02c, 02e pages.

* Refactor: 02a, syntax and typos.

* Refactor: 02c, update main-paradigm "main.scd" project file naming convention.


Version 0.1.3

* Update: add 02a - 02c pages.

* Refactor: [dxextensions](https://gitlab.com/dxarts/projects/dxextensions) as dependency.

Version 0.1.2

* Match quark names.

Version 0.1.1

* Update quark address.

Version 0.1.0

* First Public Release.


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2012-2020.

Department of Digital Arts and Experimental Media (DXARTS)  
University of Washington

&nbsp;


Contributors
------------

Version pre-release
*  Joseph Anderson (@joslloand)
*  Juan Pampin (@jpampin)
*  Joshua Parmenter (@joshpar)
*  Daniel Peterson (@dmartinp)
*  Stelios Manousakis (@Stylianos91)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts461a_wi20.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
