MainParadigm {
	var <projectPath, server, <>score, <functions, <synthDefs, <soundIn, classPath;

	*new { |projectPath = "", server, post = true|
		^super.newCopyArgs(projectPath, server).init(post);
	}

	init { |post|
		var makeLine, makeSpace, makeSynthDef, checkPost, postInfo, collectFiles;

		// Makes line in post window
		makeLine = { |length = 30|
			length.do({ "-".post }); "".postln;
		};

		// Makes space in post window
		makeSpace = { |length = 1|
			length.do({ " ".post });
		};

		// Adds CtkSynthDef for uGenGraphs
		makeSynthDef  = { |func, name|
			CtkSynthDef.new(name, func.value(this));
		};

		// Checks post statement
		checkPost = { |func|
			if (post, { func.() } );
		};

		collectFiles = { |thisFolder, folderDict|
			var folderPath;
			folderPath = PathName(projectPath +/+ thisFolder);
			checkPost.({("Loading" + thisFolder.toUpper).postln;});
			folderPath.deepFiles.do({ |thisFile|
				var file, fileName, filePath;
				fileName = thisFile.fileNameWithoutExtension;
				filePath = thisFile.fullPath;
				checkPost.({
					makeSpace.(4);
					thisFile.fileName.asSymbol.postln
				});
				if (thisFolder.asSymbol == \soundIn, {
					if (thisFile.extension != "scd", {
						file = CtkBuffer.playbuf(filePath).addTo(score)
					})
				}, {
					file = thisFile.fullPath.load
				});
				folderDict.put(fileName.asSymbol, file);
			});
			checkPost.({"".postln});
		};

		// Defines vars
		classPath = "".resolveRelative;
		score = CtkScore.new();
		functions = IdentityDictionary.new(know: true);
		synthDefs = IdentityDictionary.new(know: true);
		soundIn = IdentityDictionary.new(know: true);
		if (projectPath != "", {
			[
				["functions", functions],
				["uGenGraphs", synthDefs],
				["soundIn", soundIn]
			].do({ |thisData|
				var thisFolder, folderDict;
				thisFolder = thisData[0];
				folderDict = thisData[1];
				collectFiles.value(
					thisFolder: thisFolder,
					folderDict: folderDict
				);
			});

			synthDefs.keysValuesChange({ |key, value|
				makeSynthDef.(name: key, func: value);
			});

			postInfo = { |name, dictionary|
				makeLine.();
				name.postln;
				makeLine.();
				dictionary.keysValuesDo({ |option, data|
					makeSpace.(2); (option ++ ":" + data).postln;
					if (data.asSymbol == 'a CtkSynthDef',
						{ makeSpace.(4); "args -> ".post; data.args(post: true) }
					);
				});
				"".postln;
			};

			// Posts contents of main
			checkPost.({
				postInfo.("SoundIn", soundIn);
				postInfo.("Functions", functions);
				postInfo.("SynthDefs", synthDefs);
			});
		});

	}

	render {
		|fileName = "test", openSF = true, fileExtension = ".wav", headerFormat = "WAV", sampleFormat = "int24", sampleRate = 44100, numOutputChannels = 2, memSize = (2**20), maxSynthDefs = (2**20), maxNodes = (2**20), action = ({})|
		var options, serverOptions, path;

		fork {
			// Defines path for score to written to
			// By default score is written to the soundIn folder as wave file
			// with the given "fileName"
			path = projectPath +/+ "soundOut" +/+ fileName ++ fileExtension;

			// Defines server options
			serverOptions = ServerOptions.new
			.numOutputBusChannels_(numOutputChannels)
			.maxNodes_(maxNodes)
			.maxSynthDefs_(maxSynthDefs)
			.memSize_(memSize);

			if (score.endtime > 0, {
				// Writes score to disk
				score.write(
					path: path,
					sampleRate: sampleRate,
					headerFormat: headerFormat,
					sampleFormat: sampleFormat,
					options: serverOptions,
					action: { |err|

						// If openSF = true opens renders score
						// Else, post error code
						if (err == 0, {
							(fileName + "successfully rendered").postln;
						}, {
							("Error: " + err).postln;
						});
						if (openSF, {
							server.options_(serverOptions);
							{SFPlayer(path).gui}.defer;
						});
						action.();
					}
				)
			}, {
				"Your score is empty. Will not attempt to render".postln;
				action.();
			})
		}
	}

	makeProject { |name, path|
		var main, copyFile, functionData, uGenData;
		projectPath = path +/+ name;
		("mkdir" + path +/+ name).systemCmd;
		["functions", "uGenGraphs", "soundIn", "soundOut"].do({ |thisDir|
			("mkdir" + projectPath +/+ thisDir).systemCmd;
		});

		copyFile = { |txtFilePath, scdFilePath|
			var txt;
			File.use(txtFilePath, "r", { |f|
				txt = f.readAllString;
				txt = txt.replace("[TITLE]", name);
				txt = txt.replace("[FILENAME]", ("\"" ++ name ++ "\""))
			});
			File.use(scdFilePath, "w", {|f|
				f.write(txt);
			})
		};

		["functions", "uGenGraphs"].do({ |folder|
			PathName(classPath +/+ folder).deepFiles.do({ |thisFile|
				copyFile.(
					txtFilePath: thisFile.fullPath,
					scdFilePath: projectPath
					+/+ folder
					+/+ thisFile.fileNameWithoutExtension
					++ ".scd"
				)
			})
		});

		[
			["main.txt", (name ++ ".scd")],
			["README.txt", "README.scd"]
		].do({ |thisData|
			var txtFile, scdFile, txt;
			txtFile = thisData[0];
			scdFile = thisData[1];
			copyFile.(
				txtFilePath: classPath +/+ txtFile,
				scdFilePath: projectPath +/+ scdFile
			)
		})
	}

}


